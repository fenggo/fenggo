## 前言

Latex是科研工作中最常用的文档排版系统，然而很多Latex编辑器没有做到开箱即用，最开始使用Latex的时候经常一编译就是一堆bug，这让一个即使是计算机专业热爱折腾的人都有些望而却步，即使别人说Latex的文献管理是多么方便。造成很长一段时间，都处于：Latex真好，但我选Markdown。

后来，遇到了Overleaf。

Overleaf是开源的在线Latex编辑器软件，个人用户可以在Overleaf官网注册并免费使用Overleaf，Overleaf官网还具有Review等团队协作功能。但是Overleaf官网在国内的访问速度不佳，科学上网后速度才满足日常需求。

因此，对于科研团队来说，在自己的服务器上部署Overleaf，从此为整个团队都省去了安装Latex各种包的繁琐，多么幸福的事。需要说明的事，目前开源的个人版本的Overleaf功能没有Overleaf官网齐全，也许还有些小bug，但是就我目前的使用来说，足够日常使用了。

## 1. 安装
两种方式：下载.deb文件安装，或脚本安装。
安装必要软件，
```bash
sudo apt-get update
sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
```
### （1）. 命令或deb文件安装Docker

Docker，和docker-compose。可以通过以下命令安装。

```bash
sudo apt-get install docker-ce docker-ce-cli containerd.io
pip install docker-compose
```
Docker安装详见：
https://docs.docker.com/engine/install/ubuntu/

### (2). 脚本安装Docker：
```
sudo curl -sSL https://get.daocloud.io/docker | sh
```
查看版本，检查是否安装成功：

docker --version
docker-compose --version

启动Docker：
```bash
systemctl start docker
```

查看版本，检查是否安装成功

```bash
docker --version
docker-compose --version
```

## 2. 安装Overleaf

在用户权限下docker 命令需要 sudo 否则出现以上问题，解决方法：
通过将用户添加到docker用户组可以将sudo去掉，命令如下

```bash
sudo groupadd docker #添加docker用户组
sudo gpasswd -a $USER docker #将登陆用户加入到docker用户组中
newgrp docker #更新用户组
docker ps #测试当前用户是否可以正常使用docker命令
```

2.1安装overleaf
首先将overleaf项目从github拉至本地

```bash
git clone https://github.com/overleaf/toolkit.git ./overleaf
```
然后进行初始化配置

```bash
cd ./overleaf
bin/init
```


## 3. 配置Overleaf
这里我们进入config下的overleaf.rc文件进行配置：
```bash
vi ./config/overleaf.rc
```
需要修改的就是ports: - 80:80,一般80端口都被apache或nginx占用了，改用其他端口如：ports: - 9000:80。 其他可根据需要修改，如挂载位置等。
其中还有许多个性化的配置：如网页抬头文字内容，网址标题，UI语言（中文）等，可以在overleaf的Wiki页面中进行查看和配置。

## 4. 启动Overleaf

执行容器：bin/up。

此时正在拉取镜像，可以等出现大量的log时使用 ctrl+c 停止，然后执行bin/start即可。

此时用浏览器打开http://公网IP:映射的端口/launchpad应该能看到管理员注册界面，至此overleaf的安装结束。


## 5. 安装完整texlive

以上安装的overleaf配套的LaTeX不是完整版，所以需要继续下载。

首先进入容器的bash:
```bash
docker exec -it sharelatex bash
cd /usr/local/texlive
```

然后执行以下命令：

# 下载并运行升级脚本
```bash
wget http://mirror.ctan.org/systems/texlive/tlnet/update-tlmgr-latest.sh
sh update-tlmgr-latest.sh -- --upgrade
```

# 更换texlive的下载源
```bash
tlmgr option repository https://mirrors.sustech.edu.cn/CTAN/systems/texlive/tlnet/
```
# 升级tlmgr
```bash
tlmgr update --self --all
```
# 安装完整版texlive（时间比较长，不要让shell断开）
```bash
tlmgr install scheme-full
```

# 退出sharelatex的命令行界面，并重启sharelatex容器
```
exit
docker restart sharelatex
```

至此，安装完成。

## 6. 使用Overleaf

打开浏览器访问http://hostname:9000/launchpad，或http://127.0.0.1:9000/launchpad
创建Admin账户，愉快的玩耍吧。

linux下让ssh服务开机自动运行
1、设置ssh开机自启动

```bash
sudo systemctl enable ssh
```

说明：sudo是提升权限，systemctl是服务管理器，enable是systemctl的参数，表示启用开机自动运行，ssh是要设置的服务名称。设置成功后，可以用chkconfig查看一下ssh的开机启动状态 ，on表示已设置开机自启动。

2、ssh禁用开机自启动

```bash
sudo systemctl disable ssh
```

## 参考

[1] https://github.com/overleaf/overleaf/wiki/Quick-Start-Guide
