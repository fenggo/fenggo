# 深度学习分子动力学仿真 



[TOC]



## 分子动力学模拟


 <img src="分子动力学仿真.png" style="zoom:43%;" div align=center/> <img src="能量.png" style="zoom:72% div align=center "/> 

​        分子动力学法是一种计算机模拟实验方法，是研究凝聚态系统的有力工具。在玻恩–奥本海默近似（Born-Oppenheimer approximation，简称BO近似，又称绝热近似）条件下，将电子运动和原子核运动分离，将电子间的相互作用以原子与分子间相互作用势取代，主要是依靠牛顿力学来模拟原子与分子体系的运动。根据经典牛顿力学，

$$
m\frac{d^2r_i}{dt^2} = F_i = - \nabla V(r_i ) , 
$$

$$
(其中，r_i为第i个原子的坐标，F_i为第i个原子的受力，V为势能函数。)
$$


​        势能和每个原子的位置、类型（质量）、电荷等信息都有关。有了加速度，就可以更新每个原子的速度，进而更新位置，再返回来更新加速度。

​        由此我们得到各原子的状态（构象）随时间的变化曲线（trajectory），以此来分析动态变化过程，或者求统计平均值计算某种性质。

​        在牛顿力学下，体系一般遵循能量守恒定律。势能降低，动能就要升高。总能量和设定的环境（温度等）密切相关，对模拟影响很大。势能零点一般取平衡态。

​        分子动力学模拟的第一步是确定起始构型，一个能量较低的起始构型是进行分子模拟的基础，一般分子的起始构型主要来自实验数据或量子化学计算。在确定起始构型之后要赋予构成分子的各个原子速度，这一速度可以根据波尔兹曼分布随机生成，由于速度的分布符合波尔兹曼统计。

​       分子动力学计算的基本思想是赋予分子体系初始运动状态之后，原子、分子自然的在相空间中运动。分子动力学模拟是研究微观世界的有效手段，其势函数对模拟的精度有较大影响。

### 参考资料

* 分子模拟-从算法到应用，分子模拟的理论与实践-陈正隆等。


## 原子与分子间相互作用势

$$
\nabla V(r ) = F
$$

​        相互作用势的梯度为原子间相互作用力，常见的势模型如Morse potential，LJ potential等等，下图为一双原子分子相互作用势。

<img src="原子与分子间相互作用势.GIF" style="zoom:55%;" />



## 基于机器学习的原子与分子间相互作用势

<img src="机器学习相互作用势.png" style="zoom:55%;" />

## ReaxFF-nn机器学习势模型

Machine Learning Potential: A Combination of Reactive Force Field and Message Passing Neural Networks （Feng Guo et. al., submitted）
ReaxFF-nn: A Reactive Machine Learning Potential in GULP and the Applications in Low Dimensional Carbon nanostructures

