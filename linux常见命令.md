* 系统升级

  ```shell
  sudo apt-get update && sudo apt-get dist-upgrade -y
  ```

* tar打包压缩

  ```shell
  # 压缩
  tar -zcvf test.tar.gz ./test/
  # 解压
  tar -xzvf test.tar.gz 
  tar -xvf test.tar
  ```

* conda升级

  升级conda(升级Anaconda前需要先升级conda)：conda update -n base -c defaults conda

  升级anaconda：conda update anaconda

  重置navigator：执行anaconda-navigator --reset

  升级客户端：conda update anaconda-client

  升级安装依赖包：conda update -f anaconda-client
  conda create --name tf python=3.9 tensorflow-gpu
  conda active tf

  删除系统里的安装包：conda clean -i

  编辑源: vi ~/.condarc
```bash
  conda install -c conda-forge pyvista
```
* pip 打包

  1. python setup.py sdist
  2. 文件.pypirc输入以下信息
```bash

  [distutils]

  index-servers = pypi

  [pypi]

  username:gfeng

  password:lg*********36
```

  3. pip install twine
  4. twine upload dist/irff-*.tar.gz

* wgkb安装.deb
```bash
  sudo dpkg -i package.deb
```

ssh connection refused 
```shell
sudo apt install ssh-server
sudo service ssh start 
```